#!/bin/bash
#Clean AWS Account :
#Remove AMIs
#Remove Snapshots from multiple regions
#Validate

RED='\033[0;31m'
YELLOW='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
AWS_PROFILE=$1

#set -x


input_file=$2
# Check if the file exists
if [ -f "$input_file" ]; then
  # Read the file line by line
  while IFS= read -r region; do
    echo -e "${GREEN}[*] Processing region: $region${NC}"
    # List the snapshots in the current region
    echo -e "${GREEN}[*] Listing AWS AMIs IDs.${NC}"
    aws ec2 describe-images --owners self --region $region --query 'Images[*].[ImageId]' --output text --profile ${AWS_PROFILE}
    echo -e "${GREEN}[*] Listing AWS Snapshots.${NC}"
    aws ec2 describe-snapshots --owner-ids self --region $region --query 'Snapshots[*].[SnapshotId]' --output text  --profile ${AWS_PROFILE}
    sleep 3
    snapshots=$(aws ec2 describe-snapshots --owner-ids self --region $region --query 'Snapshots[*].[SnapshotId]' --output text  --profile ${AWS_PROFILE})

    # Check if any snapshots exist
    if [[ -n "$snapshots" ]]; then
      echo "Snapshots found in $region:"
      echo "$snapshots"
      echo -e "${YELLOW}[!!] Removing AWS AMIs IDs.${NC}"
      aws ec2 describe-images --profile ${AWS_PROFILE} --owners self --region ${region} --query 'Images[*].[ImageId]' --output text --profile ${AWS_PROFILE} | xargs -I {} aws ec2 deregister-image --image-id {}  --profile ${AWS_PROFILE}
      echo -e "${YELLOW}[!!] Removing Snapshots${NC}"
      aws ec2 describe-snapshots --profile ${AWS_PROFILE} --owner-ids self --region ${region}  --query 'Snapshots[*].[SnapshotId]' --output text  --profile ${AWS_PROFILE} | xargs -I {} aws ec2 delete-snapshot --snapshot-id {}  --profile ${AWS_PROFILE}
    else
      echo "No snapshots found in $region."
    fi
  done < "$input_file"
else
  echo "Region file $input_file not found."
fi
