packer {
  required_plugins {
    amazon = {
      version = ">= 1.2.5"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "this" {
  ami_name      = "algo-mtt"
  region        = "us-east-1"
  profile       = "demomtt"
  instance_type = "t2.micro"
  ami_regions   = ["us-east-1", "us-east-2"]
  # force_deregister      = true
  # force_delete_snapshot = true

  source_ami_filter {
    filters = {
      virtualization-type = "hvm"
      name                = "ubuntu/images/*ubuntu-xenial-16.04-amd64-server-*"
      root-device-type    = "ebs"
    }
    owners      = ["099720109477"]
    most_recent = true
  }
  ssh_username = "ubuntu"

}



build {
  name    = "demo-build"
  sources = ["source.amazon-ebs.this"]

  provisioner "shell" {
    inline = ["echo Hola Manizales Tech Talks"]
  }

  provisioner "shell-local" {
    inline = ["echo in my macbook"]
  }
}
