variable "build-region" {
  type        = string
  description = "Select the region to launch The Base Image Creation"
}

variable "profile" {
  type        = string
  description = "Select the aws cli config profile"
}

variable "ami-prefix" {
  type        = string
  description = "What is the name of the AMI that we are going to create"
}

variable "ami-regions" {
  type        = list(string)
  description = "A list of regions to copy the AMI to. Tags and attributes are copied along with the AMI."
}

variable "tags" {
  description = "A mapping of tag values to apply to AMI created resources"
  type        = map(string)
  default = {
    Project     = "ManizalesTechTalks"
    Application = "DemoApp"
    OS_Version  = "Ubuntu"
    Squad       = "InfrastructureTeam"
    cvs         = "git@gitlab.com:h3ct0rjs/manizales-tech-talks-packer.git"
  }
}

variable "instance-type" {
  type        = string
  description = "Define the instance type to be created as base image"
  default     = "t2.micro"
}
