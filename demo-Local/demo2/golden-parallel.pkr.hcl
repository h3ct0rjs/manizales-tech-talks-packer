packer {
  required_plugins {
    amazon = {
      version = ">= 1.2.5"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
  gname     = "${var.ami-prefix}-${local.timestamp}"
}

source "amazon-ebs" "demo-xenial" {
  region        = var.build-region
  profile       = var.profile
  ami_name      = "${var.ami-prefix}-xenial-${local.timestamp}"
  instance_type = var.instance-type
  ami_regions   = var.ami-regions
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-xenial-16.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  tags = merge(var.tags, {
    "Name" : "${local.gname}"
  })
  ssh_username = "ubuntu"
}

source "amazon-ebs" "demo-focal" {
  region          = var.build-region
  profile         = var.profile
  ami_name        = "${var.ami-prefix}-focal-${local.timestamp}"
  instance_type   = var.instance-type
  ami_regions     = var.ami-regions
  skip_create_ami = true #default to false.
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-focal-20.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  ssh_username = "ubuntu"
}

source "amazon-ebs" "demo-jammy" {
  region        = var.build-region
  profile       = var.profile
  ami_name      = "${var.ami-prefix}-jammy-${local.timestamp}"
  instance_type = var.instance-type

  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-jammy-22.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }

  ssh_username = "ubuntu"
}

build {
  name = "demo-parallel-build"

  sources = [
    "source.amazon-ebs.demo-xenial",
    "source.amazon-ebs.demo-focal",
    "source.amazon-ebs.demo-jammy"
  ]

  provisioner "shell" {
    environment_vars = [
      "FOO=La vida es Bella!",
    ]
    inline = [
      "echo Installing Redis",
      "sleep 30",
      "sudo apt-get update",
      "sudo apt-get install -y redis-server",
      "echo \"FOO is $FOO\" > manizalestechtalks.txt",
    ]
  }

  provisioner "shell" {
    inline = ["cat manizalestechtalks.txt"]

  }

}
