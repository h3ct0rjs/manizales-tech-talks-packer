# Demo 2: Running Packer Locally Parallels

Now that we know the three main building blocks, I need to mention that packer has a lot of great features and one of them is the possibility to copy the base image to multiple regions and also create multiple Golden images from multiple OS at the same time.

The idea for all is the same:
  - Start new EC2 instance
  Connect to that EC2 instance and executes predefined scripts amount them :
      - OS update
      - Apply OS configuration and tuning
      - Apply CIS hardening on OS
      - Install application
      - Install antivirus, IDS, IPS, file integrit check software
      - Install CloudWatch agent
      - Install Inspector agent
      - Install other agents
  - Create an AMI
  - Copy that AMI to all the regions

If we take a closer look at the code now we have three main source blocks with different operative systems :

```terraform
source "amazon-ebs" "demo-xenial" {
  ...
}
source "amazon-ebs" "demo-focal" {
  ...
}
source "amazon-ebs" "demo-jammy" {
  ...
}
```

also when we check our builder block :
```terraform
build {
  name = "demo-parallel-build"
  sources = [
    "source.amazon-ebs.demo-xenial",
    "source.amazon-ebs.demo-focal",
    "source.amazon-ebs.demo-jammy"
  ]

  provisioner "shell" {
    environment_vars = [
      "FOO=La vida es Bella!",
    ]
    inline = [
      "echo Installing Redis",
      "sleep 30",
      "sudo apt-get update",
      "sudo apt-get install -y redis-server",
      "echo \"FOO is $FOO\" > manizalestechtalks.txt",
    ]
  }

  provisioner "shell" {
    inline = ["cat manizalestechtalks.txt"]
  }

}
```

We can see that we have an array of strings containing all the sources that we are going to build.

again run the following commands from the local computer :


```sh
$ packer fmt -recursive .
$ packer validate -var-file="./dev-demo2.pkrvars.hcl" .
$ packer build -var-file="./dev-demo2.pkrvars.hcl" .
```
You will notice that when there is a parallel build the image is going to be color coded as it is shown in the following image :

![packer-2](../../img/packer-2.png)
