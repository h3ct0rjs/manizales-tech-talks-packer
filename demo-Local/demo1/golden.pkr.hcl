packer {
  required_plugins {
    amazon = {
      version = ">= 1.2.5"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

# locals {
#   timestamp = regex_replace(timestamp(), "[- TZ:]", "")
# }

source "amazon-ebs" "demo-xenial" {
  region          = var.build-region
  profile         = var.profile
  ami_name        = var.ami-prefix
  instance_type   = "t2.micro"
  skip_create_ami = true #default to false.
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-xenial-16.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }

  ssh_username = "ubuntu"
}

build {
  name = "${var.ami-prefix}-build "
  sources = [
    "source.amazon-ebs.demo-xenial"
  ]

  provisioner "shell" {
    environment_vars = [
      "FOO=Manizales Tech Talks",
    ]
    inline = [
      "echo Installing Redis",
      "sleep 30",
      "sudo apt-get update",
      "sudo apt-get install -y redis-server",
      "echo \"Hello $FOO\" > example.txt",
    ]
  }
  provisioner "shell" {
    inline = ["echo This provisioner runs last on remote machine"]
  }

  provisioner "shell-local" {
    inline = ["echo This provisioner runs last on local machine"]
  }
}
