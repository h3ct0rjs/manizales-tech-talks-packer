# Demo 1: Running Packer Locally

With this demo you will be able to validate the basic features of creating golden images. This simple example will create a Golden image based on ubuntu-xenial-16.04.

As you will see here, we have the three main building blocks:
## Packer Block:

```terraform
packer {
  required_plugins {
    amazon = {
      version = ">= 1.2.5"
      source  = "github.com/hashicorp/amazon"
    }
  }
}
```
From the official docs :

 > The previous packer configuration block type is used to configure some behaviors of Packer itself, such as the minimum required Packer version needed to apply your configuration. The **required_plugins** block specifies all of the plugins required by the current config, mapping each local plugin name to a source address and a version constrain

## Source Block:

A source block allows you to define what is the source to be used in the construction of the Golden image, this specify what is going to be , in the following example is using an amazon ebs volumen with a few specific arguments, also defining the operative systems and more.


```terraform
source "amazon-ebs" "demo-xenial" {
  region          = var.region
  profile         = var.profile
  ami_name        = var.ami-prefix
  instance_type   = "t2.micro"
  skip_create_ami = true
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-xenial-16.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }

  ssh_username = "ubuntu"
}
```

The build block defines what builders are started, how to provision them and if necessary what to do with their artifacts using post-process.

In the following case we define a build name, using the previous ebs volume and running a few provisioners like running shell scripts, passing environment vars, and running scripts in the local machine. This is useful sometimes if you want to encode a specific version o release in a specific file inside the Golden image.We want to install a redis-server and create a txt file :

```terraform
build {
  name = "${var.ami-prefix}-build "
  sources = [
    "source.amazon-ebs.demo-xenial"
  ]

  provisioner "shell" {
    environment_vars = [
      "FOO=Manizales Tech Talks",
    ]
    inline = [
      "echo Installing Redis",
      "sleep 30",
      "sudo apt-get update",
      "sudo apt-get install -y redis-server",
      "echo \"Hello $FOO\" > example.txt",
    ]
  }
  provisioner "shell" {
    inline = ["echo This provisioner runs last on remote machine"]
  }

  provisioner "shell-local" {
    inline = ["echo This provisioner runs last on local machine"]
  }
}
```

In order to test this demo1, you can run the following commands :

```sh
$ packer validate -var-file="./dev.pkrvars.hcl" .
$ packer build -var-file="./dev.pkrvars.hcl" .
```

Sometimes as we are coding we want to keep a good format, packer also offers a formating code option which is going to standarized your pkr.hcl file using the following :

```sh
$ packer fmt -recursive .
```

The dot ´.´ indicates the current path or context to pass to the packer binary.

![packer-1](../../img/packer-1.png)

If you take at look closer, in the source block we use an argument `skip_create_ami = true` this will skip the ami creation, this option is useful to test the provisioning or installation of software and also creating an stimation in time taken for packer to create the golden ami. If you want to create an ami, just comment that argument.

you will get a message like this :


```
manizales-tech-talks-build .amazon-ebs.demo-xenial: Stopping instance
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: Waiting for the instance to stop...
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: Skipping AMI creation...
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: Skipping AMI region copy...
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: Skipping Enable AMI deprecation...
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: Skipping AMI modify attributes...
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: Skipping AMI create tags...
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: Terminating the source AWS instance...
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: Cleaning up any extra volumes...
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: No volumes to clean up, skipping
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: Deleting temporary security group...
==> manizales-tech-talks-build .amazon-ebs.demo-xenial: Deleting temporary keypair...
Build 'manizales-tech-talks-build .amazon-ebs.demo-xenial' finished after 3 minutes 20 seconds.

==> Wait completed after 3 minutes 20 seconds

==> Builds finished but no artifacts were created.
```
