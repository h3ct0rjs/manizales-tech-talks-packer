variable "build-region" {
  type        = string
  description = "Select the region to launch The Base Image Creation"
}

variable "profile" {
  type        = string
  description = "Select the aws cli config profile"
}

variable "ami-prefix" {
  type        = string
  description = "What is the name of the AMI that we are going to create"
}

variable "tags" {
  description = "A mapping of tag values to apply to AMI created resources"
  type        = map(string)
  default = {
    "Project" = "ManizalesTechTalks"
  }
}
