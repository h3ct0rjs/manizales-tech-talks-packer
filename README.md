# Manizales Tech Talk: Building AWS Golden Images using Packer and Gitlab
<img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg?logo=readthedocs&logoColor=white&style=flat-square" />
<img alt="License: MIT" src="https://img.shields.io/badge/license-MIT-yellow.svg?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgAQMAAABJtOi3AAAABlBMVEUAAAD///+l2Z/dAAAAAXRSTlMAQObYZgAAAHpJREFUCNdjYOD/wMDAUP+PgYHxhzwDA/MB5gMM7AwMDxj4GBgKGGQYGCyAEEgbMDDwAAWAwmk8958xpIOI5zKH2RmOyhxmZjguAiKmgIgtQOIYmFgCIp4AlaQ9OczGkJYCJEAGgI0CGwo2HmwR2Eqw5SBnNIAdBHYaAJb6KLM15W/CAAAAAElFTkSuQmCC&style=flat-square" />

Building and managing standardized Amazon Web Services (AWS) machine images, known as Golden images, Master Images
is a crucial aspect of modern cloud infrastructure management. In this talk, we will explore how to
use Packer and GitLab to create and maintain golden images efficiently and automatically.

We will start with an overview of golden images and why they are essential for AWS infrastructure management.

Then, we will dive into the specifics of using [Packer](https://www.packer.io/), a popular open-source tool for
creating machine images,to build your golden images. We will cover the key concepts and best practices for creating
Packer templates that can be used to build images consistently across different AWS regions and accounts.

I'll demonstrate how GitLab CI/CD can automate the image-building process and also deploying to AWS.

By the end of this talk, you will have a solid understanding of using Packer and GitLab to create,
manage, and automate your AWS golden image pipeline, enabling you to maintain a scalable and secure cloud infrastructure.

## Requirements

Install packer-cli tool, if you're using a macbook this can be done by executing the following commands:

```bash
  $ brew tap hashicorp/tap
  $ brew install hashicorp/tap/packer
```
If you're using another OS, check [here](https://developer.hashicorp.com/packer/downloads)

## Build Demos

This repo assumes that you have setup the AWS CLI Secrets to interact with AWS.
In order to run the Demos you will need to do the following

```bash
  $ cd demoX
  $ packer build golden.pkr.hcl
```

Where demo`X` will be one of the available demos

## Pre-commit Rules(optional)

This repo implements pre-commit in order to keep code fmted you can install that following the
official instructions here :

  > https://pre-commit.com/#install

after installing it you will have a binary in your system and the you can run

```bash
  $ pre-commit install
  $ pre-commit run
```


## Useful Commands

If you want to debug packer, this can be achieved using the following environment variables :
```bash
export PACKER_LOG=1
export PACKER_LOG_PATH="packerlog.txt"
```

Here is a list of possible useful commands in your packer journey :
```bash
$ packer init .  #Initialize plugins
$ packer fmt -check . # Only check if format linting is needed.
$ packer fmt -recursive . # Do all the format and linting changes.

$ packer validate hello.pkr.hcl # Validate a specific file

$ packer validate -var-file="./dev.pkrvars.hcl" # pass a var-file

$ packer build -var-file="./dev.pkrvars.hcl" . # build with a var file

$ packer inspect  -var-file="dev.pkrvars.hcl" .

$ packer plugins installed
```

## Slides

The presentation for this tech talk can be found in the **slides folder**, there is also a copy of this online in the following link :

[https://slides.com/c1b3r/packer-images](https://slides.com/c1b3r/packer-images)

## Useful Resources

You can get additional information from the following resources :
- https://www.hashicorp.com/resources/automating-way-out-federal-data-centers-into-cloud-automated-image
- https://developer.hashicorp.com/packer/tutorials/cloud-production/multicloud
- https://www.redhat.com/en/topics/linux/what-is-a-golden-image
- https://github.com/chef/bento/tree/main/packer_templates
- https://github.com:cisagov/guacamole-packer.git a sample of the same concept using Github
- https://developer.hashicorp.com/packer/docs/debugging
- https://www.ansible.com/blog/ansible-and-packer-why-they-are-better-together
### HCL Documentation :

- https://developer.hashicorp.com/packer/guides/hcl/variables
- https://github.com/hashicorp/hcl2/blob/master/hcl/hclsyntax/spec.md

## Acknowledgements

Thanks to Manizales Tech Talks member, specially to Jhon Edison Castro for the invite.

## Authors

- [@h3ct0rjs](https://www.github.com/h3ct0rjs)
- [devops.com.co](https://devops.com.co)
