packer {
  required_plugins {
    amazon = {
      version = ">= 1.2.5"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "create_ami" {
  type        = bool
  description = "If true, Packer will not create the AMI. Useful for setting to true during a build test stage. Default false"
  default     = false
}
variable "distribute_ami_regions" {
  type        = list(string)
  description = "This is a list that can be used to distribute the ami across multiple regions"
  default     = ["us-east-1"]
}

source "amazon-ebs" "this" {
  ami_name      = "demo-manizales-tech-talks"
  region        = "us-east-1"
  profile       = "demomtt"
  instance_type = "t2.micro"
  ami_regions   = var.distribute_ami_regions
  #Remove Image with the same name
  skip_create_ami       = var.create_ami
  force_deregister      = true
  force_delete_snapshot = true

  source_ami_filter {
    filters = {
      virtualization-type = "hvm"
      name                = "ubuntu/images/*ubuntu-xenial-16.04-amd64-server-*"
      root-device-type    = "ebs"
    }
    owners      = ["099720109477"]
    most_recent = true
  }
  ssh_username = "ubuntu"

  #Block device Configurations:
  launch_block_device_mappings {
    device_name           = "/dev/sda1"
    volume_size           = 8
    volume_type           = "gp2"
    delete_on_termination = true
  }
  tags = {
    Name = "demo-ami-mtt"
  }
}

build {
  name = "demo-build"

  sources = ["source.amazon-ebs.this"]

  provisioner "shell" {
    inline = [
      "echo Hello Manizales Tech Talks Community",
      "echo other"
    ]
  }

  provisioner "shell" {
    script = "./scripts/provisioner.sh"
  }

  provisioner "ansible" {
    playbook_file = "./playbooks/playbook.yml"
    use_proxy     = false
  }
}
