terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.0.1"
    }
  }
}

provider "aws" {
  # Configuration options
  region = var.region

}


resource "aws_instance" "vm" {
  # We use a simple data resource to search for our previous img
  ami                    = data.aws_ami.packer-image.id
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.test_http.id]

  tags = merge(var.tags, {
    Name = var.name_tag,
  })
}

resource "aws_security_group" "test_http" {
  name        = "test_sg"
  description = "Security group for the test instance, open 80 port"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "test_sg"
  }
}
