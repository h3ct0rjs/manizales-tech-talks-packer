
variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "Name of the EC2 instance to run test"
}

variable "region" {
  type        = string
  description = "Region to deploy the packer image"
  default     = "us-east-1"
}

variable "tags" {
  description = "A mapping of tag values to apply to AMI created resources"
  type        = map(string)
  default = {
    Project     = "ManizalesTechTalks"
    Application = "DemoApp"
    OS_Version  = "Ubuntu"
    Squad       = "InfrastructureTeam"
    cvs         = "git@gitlab.com:devopscol/packer-manizales-tech-talks.git"
  }
}
