data "aws_ami" "packer-image" {

  most_recent = true
  owners      = ["self"]

  filter {
    name   = "name"
    values = ["demo-manizales-tech-talks"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
