# Demos using Gitlab CICD

This demo is just an example to check how you can use Gitlab CICD to build and deploy your Golden images.

We start by creating a .gitlab-ci.yml file,with this you will be creating the stages and all the jobs that are required.


```yaml
stages:
  - pre-check
  - lint
  - build
  - deploy-lower
  - distribute
```

Each step of the pipeline is in charge of doing multiple things.

the gitlab cicd configuration is intended to show the basic usage, there are multiple things that can be improve in the template, but that's one of the important things that you will need to validate 😃 in your learning process.
